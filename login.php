<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day 2</title>
    <link rel="stylesheet" href="./login.css">
</head>
<body>
    <div class="center">
        <form action="" method="post">
            <div class="date">
                <?php
                    date_default_timezone_set('Asia/Ho_Chi_Minh');
                    echo date('\B\â\y \g\i\ờ \l\à\: \ H:i \n\g\à\y\ d/m/Y');
                ?> <br>
            </div>
            <div class="name">
                <span><?php echo "Tên đăng nhập"; ?></span>
                <input id="name" name="name" type="text" value=""><br>
            </div>

            <div class="pass">
                <span><?php echo "Mật khẩu"; ?></span>
                <input id= "pass" name="pass" type="password" value=""><br>
            </div>
        
            <button type="submit" name="submit"><?php echo "Đăng nhập"; ?></button>
        </form>
    </div>
</body>
</html>